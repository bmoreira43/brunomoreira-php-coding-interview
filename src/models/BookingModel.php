<?php

namespace Src\models;
use Src\helpers\Helpers;
use Src\models\ClientModel;

class BookingModel {

	private $bookingData;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking($data) {
		$bookings = $this->getBookings();

		$data['id'] = end($bookings)['id'] + 1;
		$bookings[] = $data;

		// $client_model = new ClientModel;

		// if (){
		// 	$data['price']
		// }

		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}
}