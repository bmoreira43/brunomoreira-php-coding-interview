<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel {

	private $dogData;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/dogs.json');
		$this->dogData = json_decode($string, true);
	}

	public function getDogs() {
		return $this->dogData;
	}

	public function getAvaregaAge($client_id) {
		$results = $this->getDogs();

		$average_age = 0;
		$counter = 0;

		foreach ($results as $result ) {
			if($client_id === $result['clientid']) {
				$average_age = $average_age + $result['age'];
				$counter = $counter + 1;
			}
		}

		$final_age = $average_age / $counter;
		return $final_age;
	}
}